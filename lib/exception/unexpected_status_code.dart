class UnexpectedStatusCode implements Exception {
  final int? got;
  final int want;

  UnexpectedStatusCode({
    required this.want,
    required this.got,
  });
}
