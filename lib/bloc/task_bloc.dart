import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:upliftingpark/model/post_task.dart';
import 'package:upliftingpark/model/task.dart';
import 'package:upliftingpark/repository/i_task_repository.dart';

part 'task_bloc.freezed.dart';

@freezed
class TaskEvent with _$TaskEvent {
  const TaskEvent._();

  const factory TaskEvent.loadingStarted() = _LoadingStartedTaskEvent;
  factory TaskEvent.added(PostTask task) = _AddedTaskEvent;
}

@freezed
class TaskState with _$TaskState {
  const TaskState._();

  bool get isLoading =>
      this is _InitialTaskState || this is _LoadInProgressTaskState;

  const factory TaskState.initial() = _InitialTaskState;
  const factory TaskState.loadInProgress() = _LoadInProgressTaskState;
  const factory TaskState.loadFail() = _LoadFailTaskState;
  factory TaskState.loadSuccess({required List<Task> tasks}) =
      _LoadSuccessTaskState;
}

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  final ITaskRepository repository;

  TaskBloc({required this.repository}) : super(const TaskState.initial()) {
    on<TaskEvent>(
      (event, emit) => event.map<Future<void>>(
        loadingStarted: (event) => _loadTasks(event, emit),
        added: (event) => _addTask(event, emit),
      ),
      transformer: sequential(),
    );
  }

  Future<void> _loadTasks(
      _LoadingStartedTaskEvent event, Emitter<TaskState> emit) async {
    emit(const TaskState.loadInProgress());
    try {
      List<Task> tasks = await repository.get();
      emit(TaskState.loadSuccess(tasks: tasks));
    } on Object {
      emit(const TaskState.loadFail());
    }
  }

  Future<void> _addTask(_AddedTaskEvent event, Emitter<TaskState> emit) async {
    try {
      await repository.add(event.task);
      add(const TaskEvent.loadingStarted());
    } on Object {
      emit(const TaskState.loadFail());
    }
  }
}
