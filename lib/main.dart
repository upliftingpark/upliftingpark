import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:upliftingpark/bloc/task_bloc.dart';
import 'package:upliftingpark/config.dart';
import 'package:upliftingpark/model/post_task.dart';
import 'package:upliftingpark/repository/task_repository.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue
          // colorScheme: const ColorScheme(
          //     background: Colors.white,
          //     brightness: Brightness.light,
          //     error: Colors.pink,
          //     onBackground: Colors.black,
          //     onError: Colors.white,
          //     onPrimary: Colors.black,
          //     onSecondary: Colors.black,
          //     onSurface: Colors.black,
          //     primary: Colors.white,
          //     secondary: Colors.blue,
          //     surface: Colors.blue),
          ),
      home: BlocProvider(
        create: (context) {
          final bloc = TaskBloc(
            repository: TaskRepository(
              dio: Dio(
                BaseOptions(
                  baseUrl: serverURL,
                ),
              ),
            ),
          );
          bloc.add(const TaskEvent.loadingStarted());
          return bloc;
        },
        child: const MyHomePage(title: 'Uplifting park'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final taskNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: BlocBuilder<TaskBloc, TaskState>(builder: (context, state) {
        return state.when<Widget>(initial: () {
          return const Center(child: CircularProgressIndicator());
        }, loadInProgress: () {
          return const Center(child: CircularProgressIndicator());
        }, loadFail: () {
          return ElevatedButton(
              onPressed: () {
                BlocProvider.of<TaskBloc>(context)
                    .add(const TaskEvent.loadingStarted());
              },
              child: const Text('Try again'));
        }, loadSuccess: (tasks) {
          return RefreshIndicator(
            onRefresh: () {
              final tasksBloc = BlocProvider.of<TaskBloc>(context);
              tasksBloc.add(const TaskEvent.loadingStarted());
              return tasksBloc.stream
                  .firstWhere((element) => element.isLoading);
            },
            child: Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return ListTile(title: Text(tasks[index].name));
                    },
                    itemCount: tasks.length,
                  ),
                ),
                Container(
                    color: const Color(0xFFFAF8F8),
                    padding:
                        const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                    child: Row(
                      children: [
                        Expanded(
                            child: TextField(
                                controller: taskNameController,
                                decoration: const InputDecoration(
                                    border: InputBorder.none))),
                        IconButton(
                            onPressed: () {
                              BlocProvider.of<TaskBloc>(context).add(
                                TaskEvent.added(
                                  PostTask(
                                      name: taskNameController.text,
                                      complete: false),
                                ),
                              );
                            },
                            icon: const Icon(Icons.send))
                      ],
                    )),
              ],
            ),
          );
        });
      }),
    );
  }
}
