// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'post_task.freezed.dart';
part 'post_task.g.dart';

/// PostTask data class
@freezed
class PostTask with _$PostTask {
  const PostTask._();

  /// PostTask
  const factory PostTask({
    required String name,
    required bool complete,
  }) = _PostTask;

  /// Generate PostTask class from Map<String, Object?>
  factory PostTask.fromJson(Map<String, Object?> json) =>
      _$PostTaskFromJson(json);
}
