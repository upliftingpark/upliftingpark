import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:upliftingpark/exception/unexpected_status_code.dart';
import 'package:upliftingpark/model/post_task.dart';
import 'package:upliftingpark/model/task.dart';
import 'package:upliftingpark/repository/i_task_repository.dart';

class TaskRepository implements ITaskRepository {
  final Dio dio;

  TaskRepository({
    required this.dio,
  });

  @override
  Future<List<Task>> get() async {
    try {
      final response = await dio.get('/tasks');

      if (response.statusCode == 200) {
        List<Task> tasks = (response.data as List<dynamic>)
            .map((e) => Task.fromJson(e))
            .toList();

        return tasks;
      }

      throw UnexpectedStatusCode(want: 200, got: response.statusCode);
    } on Object catch (e) {
      log('error while getting tasks $e');
      rethrow;
    }
  }

  @override
  Future<void> add(PostTask task) async {
    try {
      final response = await dio.post(
        '/tasks',
        data: task.toJson(),
        options: Options(responseType: ResponseType.plain),
      );

      if (response.statusCode == 201) {
        return;
      }

      throw UnexpectedStatusCode(want: 201, got: response.statusCode);
    } on Object catch (e) {
      log('error while getting tasks $e');
      rethrow;
    }
  }
}
