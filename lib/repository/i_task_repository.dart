import 'package:upliftingpark/model/post_task.dart';
import 'package:upliftingpark/model/task.dart';

abstract class ITaskRepository {
  Future<List<Task>> get();
  Future<void> add(PostTask task);
}
